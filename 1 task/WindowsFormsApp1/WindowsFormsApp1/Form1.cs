﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount=(int)numericUpDown1.Value;
        }
        public int sumVector()
        {
            int sum = 0;
            foreach (DataGridViewCell cell in dataGridView1.CurrentRow.Cells)
            {
                sum += Convert.ToInt32(cell.Value);
            }
            return sum;
        }
        public double getAverage()
        {
            return sumVector()/(int)numericUpDown1.Value;
        }
        public void changeVector()
        {
            //sumVector();
            double avg=getAverage();
            foreach (DataGridViewCell cell in dataGridView1.CurrentRow.Cells)
            {
                if (Convert.ToInt32(cell.Value)<avg)
                {
                    cell.Value = 0;
                }
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            changeVector();
        }
    }
}
