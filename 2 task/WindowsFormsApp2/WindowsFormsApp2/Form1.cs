﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        public double[] getArray()
        {
            int rowCount = dataGridView1.Rows.Count - 1;
            int columnCount = dataGridView1.Columns.Count;
            double[] array = new double[rowCount * columnCount];

            int index = 0;
            for (int i = 0; i < rowCount; i++)
            {

                for (int j = 0; j < columnCount; j++)
                {

                    double cellValue = Convert.ToDouble(dataGridView1.Rows[i].Cells[j].Value);
                    array[index] = cellValue;
                    index++;
                }
            }
            return array;

        }
        public void printArr(double[] array)
        {
            string s = "Одновимірний масив:[";
            for (int i = 0; i < array.Length; i++)
            {
                s += array[i] + ",";
            }
            label1.Text = s + "]";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            double[] array = getArray();
            printArr(array);

           
            

            
            
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.RowCount = Convert.ToInt32(numericUpDown1.Value);
            
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = Convert.ToInt32(numericUpDown2.Value);
        }
        
    }
}
